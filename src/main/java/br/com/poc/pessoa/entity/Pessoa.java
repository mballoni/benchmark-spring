package br.com.poc.pessoa.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "PESSOA")
@NamedQueries(value =
        @NamedQuery(name = Pessoa.FIND_BY_DOCUMENT,
                query = "FROM Pessoa p WHERE p.document = :" + Pessoa.PARAM_DOCUMENT))
public class Pessoa implements Serializable {

    public static final String FIND_BY_DOCUMENT = "GET_BY_DOCUMENT";
    public static final String PARAM_DOCUMENT = "document";

    private static final long serialVersionUID = -8211841012795614474L;

    @Id
    @GeneratedValue(generator = "SEQ_PESSOA", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(sequenceName = "SEQ_PESSOA", name = "SEQ_PESSOA")
    private int id;

    @Column(name = "NOME")
    private String nome;

    @Column(name = "DOCUMENT", unique = true)
    private String document;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    @Override
    public String toString() {
        return "Pessoa [id=" + id + ", nome=" + nome + ", document=" + document + "]";
    }

}
