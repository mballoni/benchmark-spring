package br.com.poc.pessoa.repository.jpa;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.poc.pessoa.entity.Pessoa;
import br.com.poc.pessoa.repository.PessoaDAO;

@Repository
public class PessoaDAOImpl implements PessoaDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public Pessoa find(String document) {
        return em.createNamedQuery(Pessoa.FIND_BY_DOCUMENT, Pessoa.class)
                .setParameter(Pessoa.PARAM_DOCUMENT, document)
                .getSingleResult();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void save(Pessoa pessoa) {
        em.persist(pessoa);
    }

}
