package br.com.poc.pessoa.repository.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.poc.pessoa.entity.Pessoa;
import br.com.poc.pessoa.repository.PessoaDAO;

@Repository
public class PessoaDAOImpl implements PessoaDAO {

    @Autowired
    private DataSource dataSource;

    @Override
    public Pessoa find(String document) {
        String sql = "SELECT * FROM PESSOA WHERE DOCUMENT = ?";

        try (Connection con = dataSource.getConnection()) {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, document);
            ps.setMaxRows(1);
            ResultSet rs = ps.executeQuery();
            Pessoa pessoa = new Pessoa();
            while (rs.next()) {
                pessoa.setDocument(rs.getString("DOCUMENT"));
                pessoa.setId(rs.getInt("id"));
                pessoa.setNome(rs.getString("NOME"));
            }
            rs.close();
            ps.close();

            return pessoa;
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void save(Pessoa pessoa) {
        String sql = "INSERT INTO PESSOA (NOME, DOCUMENT) VALUES (?, ?)";

        try (Connection con = dataSource.getConnection()) {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, pessoa.getNome());
            ps.setString(2, pessoa.getDocument());
            ps.executeUpdate();
            ps.close();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

}
