package br.com.poc.pessoa.repository;

import br.com.poc.pessoa.entity.Pessoa;

public interface PessoaDAO {

    Pessoa find(String document);

    void save(Pessoa pessoa);
}
