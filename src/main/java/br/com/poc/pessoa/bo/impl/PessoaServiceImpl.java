package br.com.poc.pessoa.bo.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.poc.pessoa.bo.PessoaService;
import br.com.poc.pessoa.entity.Pessoa;
import br.com.poc.pessoa.facade.dto.PessoaDTO;
import br.com.poc.pessoa.repository.PessoaDAO;

@Component
public class PessoaServiceImpl implements PessoaService {

    @Autowired
    private PessoaDAO pessoaDAO;

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void save(PessoaDTO chamada) {
        Pessoa pessoa = new Pessoa();
        pessoa.setNome(chamada.getNome());
        pessoa.setDocument(chamada.getCpf());

        pessoaDAO.save(pessoa);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
    public Pessoa find(String document) {
        return pessoaDAO.find(document);
    }

}
