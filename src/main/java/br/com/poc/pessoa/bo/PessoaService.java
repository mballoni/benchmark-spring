package br.com.poc.pessoa.bo;

import br.com.poc.pessoa.entity.Pessoa;
import br.com.poc.pessoa.facade.dto.PessoaDTO;

public interface PessoaService {

    void save(PessoaDTO chamada);

    Pessoa find(String document);

}
