package br.com.poc.pessoa.facade.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.poc.pessoa.bo.PessoaService;
import br.com.poc.pessoa.entity.Pessoa;
import br.com.poc.pessoa.facade.PessoaServiceFacade;
import br.com.poc.pessoa.facade.dto.PessoaDTO;

@RestController
@RequestMapping(value = "/pessoa")
public class PessoaServiceFacadeImpl implements PessoaServiceFacade {

    @Autowired
    private PessoaService pessoaService;

    @Override
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void create(@RequestBody PessoaDTO chamada) {
        pessoaService.save(chamada);
    }

    @Override
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Pessoa find(@RequestParam("document") String document) {
        return pessoaService.find(document);
    }
}
