package br.com.poc.pessoa.facade;

import br.com.poc.pessoa.entity.Pessoa;
import br.com.poc.pessoa.facade.dto.PessoaDTO;

public interface PessoaServiceFacade {

    public void create(PessoaDTO chamada);

    public Pessoa find(String document);
}
